//
// Copyright (C) 2017 Project Garibaldi
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software Foundation,
// Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
//

#pragma once

#include <Urho3D/Container/Str.h>

using namespace Urho3D;

class CPop
{
public:
	CPop() {};
	CPop(unsigned province, String name, String religion, String culture, float employed, float literate);

	bool operator!=(const CPop a);
public:
	unsigned province_key;
	String name;
	String religion;
	String culture;
	
	// percent of the pop that is:
	float employed;
	float literate;
	
	//number of each resource the pop has
	EResource stock[getResourceEnum(NULL)];
};












