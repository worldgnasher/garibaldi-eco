//
// Copyright (C) 2017 Project Garibaldi
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software Foundation,
// Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
// 


#include "CPop.h"
const decimal Euler = std::exp(1.0);


CPop::CPop(unsigned province, String name, String religion, String culture, float employed, float literate, EResource stock[getResourceEnum(NULL)])
	: province_key(province), name(name), religion(religion), culture(culture), employed(employed), literate(literate), stock(stock)
{
}

bool CPop::operator!=(const CPop a)
{
	return (a.name.Compare(name) != 0) || (a.province_key != province_key) || (a.employed != employed) || (a.literate != literate) || (a.religion.Compare(religion) != 0) || (a.culture.Compare(culture) != 0);
}


//CAlculate a pop's
float CPop::BuyProb(EResource resource, CPop pop)
{
    place=getResourceEnum(resource);
	
	//calculate price pops will pay 
	unsigned province=Cpop.province_key;
	unsigned country= /*country province is in */;
	float tariff= country./*tariff*/;
	
	//find the best price
	float price= /*first production cost*/ * /*country tariff*/+ TransportCosts + 0.3*pop.PurchaseProb[place];
	bestprice=price;
	for( int x=0 ; x< /*length of production cost vector*/ ; x++)
	{	
		if(price<bestprice)
		{
			bestprice=price;
		}
		
		float price= /*production cost x*/ * /*country tariff*/+ TransportCosts + 0.3*pop.PurchaseProb[place];
	}
		
	//calculate the Purchase Probability
	float Z=pop.PurchaseProb[place]+0.2*desirability-0.5*stockpile-0.1*bestprice;
	pop.PurchaseProb[place]=1/(1-euler^(-Z));
	
	//see if the pop has enough money to buy the resource
	if(pop.money<bestprice*pop.PurchaseProb[place])
	{
		pop.PurchaseProb[place]=price/pop.money;
	}
	
	return pop.PurchaseProb;
}





































